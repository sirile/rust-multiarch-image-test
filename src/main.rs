extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate simple_logger;

use actix_web::{get, App, HttpResponse, HttpServer, Responder};
use gethostname::gethostname;
use lazy_static::lazy_static;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

lazy_static! {
    pub static ref HOSTNAME: String = gethostname().into_string().unwrap();
    pub static ref HASH: String = hash();
}

// Calculate a color-hash based on the hostname
fn hash() -> String {
    let mut hasher = DefaultHasher::new();
    (*HOSTNAME).hash(&mut hasher);
    return r"#".to_string() + &format!("{:.6x}", hasher.finish())[..6];
}

#[get("/")]
async fn yingyang() -> impl Responder {
    info!("Served image, host: {}, colour: {}", *HOSTNAME, *HASH);
    HttpResponse::Ok().body(format!(
        "<!DOCTYPE html><html><head>
    <title>Rust scaling demo!</title></head><body>
    <h1>{}</h1>
    <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 400 400\">
    <circle cx=\"50\" cy=\"50\" r=\"48\" fill=\"{}\" stroke=\"#000\"/>
    <path d=\"M50,2a48,48 0 1 1 0,96a24 24 0 1 1 0-48a24 24 0 1 0 0-48\" fill=\"#000\"/>
    <circle cx=\"50\" cy=\"26\" r=\"6\" fill=\"#000\"/>
    <circle cx=\"50\" cy=\"74\" r=\"6\" fill=\"#FFF\"/>
    </svg>
    </body></html>",
        *HOSTNAME, *HASH
    ))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    simple_logger::init_with_level(log::Level::Info).unwrap();
    info!("Serving image, host: {}, colour: {}", *HOSTNAME, *HASH);
    HttpServer::new(|| App::new().service(yingyang))
        .bind("[::]:8080")?
        .run()
        .await
}

#[cfg(test)]
mod tests {
    use actix_web::{test, App};

    use super::*;

    #[actix_web::test]
    async fn test_index_get() {
        simple_logger::init_with_level(log::Level::Info).unwrap();
        let app = test::init_service(App::new().service(yingyang)).await;
        let req = test::TestRequest::get().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }
}
