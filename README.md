# Rust multiarch example with GitLab CI/CD

This example project returns a yin and yang-image with colors based on the hostname. It can be used to demonstrate scaling in various runtime environments, for example in a Kubernetes cluster. It demonstrates minimal container size by using a statically linked executable in a scratch-container.

GitLab CI/CD-pipelines are used to run unit tests as well as to build multiarchitecture image for AMD64 and ARM64.

## Commands

### Run locally

```bash
cargo run
```

### Run tests

```bash
cargo test
```

### Get coverage report

First make sure that the llvm-cov is installed

```bash
cargo +stable install cargo-llvm-cov --locked
```

After that the coverage tests can be run with

```bash
cargo llvm-cov
```

The html report can be created and opened with

```bash
cargo llvm-cov --html
open target/llvm-cov/html/index.html
```

### Local multiarchitecture Docker build and push

```bash
docker buildx build --platform linux/amd64,linux/arm64 -t sirile/rust-multiarch-image-test --output type=registry .
```

### Running from DockerHub

```bash
docker run --rm -p 8080:80 sirile/rust-multiarch-image-test 
```

### Running from GitLab registry

```bash
docker run --rm -p 8080:8080 registry.gitlab.com/sirile/rust-multiarch-image-test
```

### Accessing the local image

Point the browser to <http://localhost:8080>.

## TODO

- ..
