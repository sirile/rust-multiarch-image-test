ARG appname=rust-multiarch-image-test

# Build
FROM --platform=$BUILDPLATFORM rust:1.84 AS rust-builder

RUN apt-get update && apt-get install -y \
    g++-x86-64-linux-gnu libc6-dev-amd64-cross \
    g++-aarch64-linux-gnu libc6-dev-arm64-cross && \
    rm -rf /var/lib/apt/lists/*
RUN rustup target add x86_64-unknown-linux-musl aarch64-unknown-linux-musl
ENV CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_LINKER=x86_64-linux-gnu-gcc \
    CC_x86_64_unknown_linux_musl=x86_64-linux-gnu-gcc \
    CXX_x86_64_unknown_linux_musl=x86_64-linux-gnu-g++ \
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER=aarch64-linux-gnu-gcc \
    CC_aarch64_unknown_linux_musl=aarch64-linux-gnu-gcc \
    CXX_aarch64_unknown_linux_musl=aarch64-linux-gnu-g++ \
    CARGO_INCREMENTAL=0

FROM --platform=$BUILDPLATFORM rust-builder AS build-amd64
ARG appname
WORKDIR /app
COPY ./src src
COPY ./Cargo.lock Cargo.lock
COPY ./Cargo.toml Cargo.toml
RUN cargo build --release --target x86_64-unknown-linux-musl
RUN mv /app/target/x86_64-unknown-linux-musl/release/${appname} /app/${appname}

FROM --platform=$BUILDPLATFORM rust-builder AS build-arm64
ARG appname
WORKDIR /app
COPY ./src src
COPY ./Cargo.lock Cargo.lock
COPY ./Cargo.toml Cargo.toml
RUN cargo build --release --target aarch64-unknown-linux-musl
RUN mv /app/target/aarch64-unknown-linux-musl/release/${appname} /app/${appname}

FROM scratch AS final-amd64
ARG appname
EXPOSE 8080
COPY --from=build-amd64 /app/${appname} executable
CMD ["./executable"]

FROM scratch AS final-arm64
ARG appname
EXPOSE 8080
COPY --from=build-arm64 /app/${appname} executable
CMD ["./executable"]

FROM final-${TARGETARCH}